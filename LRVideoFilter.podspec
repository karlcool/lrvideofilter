Pod::Spec.new do |s|
  s.name         = "LRVideoFilter"
  s.version      = "1.3.7"
  s.summary      = "FFmpeg视频实时滤镜"
  s.homepage     = "https://gitlab.com/karlcool/lrvideofilter"
  s.license      = 'GPL'
  s.platform     = :ios
  s.requires_arc = true
  s.author       = { "karlcool.l" => "karlcool.l@qq.com" }
  s.source       = { :git => "https://gitlab.com/karlcool/lrvideofilter.git", :tag => "#{s.version}" }
  s.public_header_files = 'Class/kit/*.{h}'
  s.source_files  = 'Class/**/*.{h,m,mm,cpp}'
  s.ios.deployment_target = '9.0'
  s.frameworks = 'AVFoundation', 'Accelerate', 'AudioToolbox', 'CoreAudio', 'CoreGraphics', 'CoreMedia', 'MediaPlayer', 'QuartzCore', 'VideoToolbox'
  s.libraries = 'iconv', 'bz2', 'z', 'c++'
  s.vendored_frameworks = 'FFmpeg.framework'

  #发布时一定要加上--use-libraries否则编译不通过
end