//
//  ViewController.m
//  LRVideoFilter
//
//  Created by 刘彦直 on 2019/3/22.
//  Copyright © 2019 刘彦直. All rights reserved.
//

#import "ViewController.h"
#import "FFMpegTool.h"
#import "PixelPlayer.h"
#import "RTFilter.h"
#import "VideoInfo.h"
#import "AVFrameTool.h"

@interface ViewController () <PixelPlayerDelegate> {
    RTFilter *filter;
    VideoInfo *videoInfo;
}

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSString *outPath = @"/Users/karlcool/Desktop/270d2.mp4";
    NSString *inPath = [NSBundle.mainBundle pathForResource:@"90d" ofType:@"mp4"];
    NSURL *inURL = [NSURL fileURLWithPath:inPath];
    
    videoInfo = [[VideoInfo alloc] initWithURL:inURL];
    
    FFMpegTool *tool = [[FFMpegTool alloc] initWithIn:inPath out:outPath info: videoInfo];
    
    WaterMark *wm1 = [[WaterMark alloc] initWithPath:[NSBundle.mainBundle pathForResource:@"w4" ofType:@"jpg"] rect:CGRectMake(100, 50, 200, 100)];
    WaterMark *wm2 = [[WaterMark alloc] initWithPath:[NSBundle.mainBundle pathForResource:@"w1" ofType:@"jpg"] rect:CGRectMake(0, 100, 100, 100)];
    
//    [tool doTranslateWithWaterMarks:@[wm1, wm2] filter:[[RTFilter alloc] initWithDesc:@"[in]split [main][tmp]; [tmp] crop=iw:ih/2:0:0, vflip [flip]; [main][flip] overlay=0:H/2[out]" width:videoInfo.width height:videoInfo.height] keepAuido:false completion:^(BOOL succeed, NSString * _Nonnull outPath) {
//        NSLog(@"写入完成");
//    }];
    
        PixelPlayer *player = [[PixelPlayer alloc] initWithURL:inURL];
        player.frame = self.view.bounds;
        player.delegate = self;
        player.isLoop = YES;
        [self.view addSubview:player];

        [player play];
    
//    player.progressChanged = ^(CGFloat progress) {
//
//    }
//    player.progressChanged = ^(CGFloat progress) {
//        NSLog(@"%f",progress);
//    };
    
    UIButton *btn1 = [[UIButton alloc] initWithFrame:CGRectMake(0, 80, 40, 40)];
    btn1.backgroundColor = [UIColor redColor];
    [btn1 addTarget:self action:@selector(filter1) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn1];
    
    UIButton *btn2 = [[UIButton alloc] initWithFrame:CGRectMake(0, 120, 40, 40)];
    btn2.backgroundColor = [UIColor blueColor];
    [btn2 addTarget:self action:@selector(filter2) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn2];
    
    UIButton *btn3 = [[UIButton alloc] initWithFrame:CGRectMake(0, 160, 40, 40)];
    btn3.backgroundColor = [UIColor greenColor];
    [btn3 addTarget:self action:@selector(filter3) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:btn3];
}

- (void)filter1 {
    filter = [[RTFilter alloc] initWithDesc:@"[in]lutyuv='y=negval:u=negval:v=negval'[out]" width:videoInfo.width height:videoInfo.height];
}

- (void)filter2 {
    filter = [[RTFilter alloc] initWithDesc:@"[in]boxblur=3[out]" width:videoInfo.width height:videoInfo.height];
}

- (void)filter3 {
    filter = [[RTFilter alloc] initWithDesc:@"[in]split [main][tmp]; [tmp] crop=iw:ih/2:0:0, vflip [flip]; [main][flip] overlay=0:H/2[out]" width:videoInfo.width height:videoInfo.height];
}

- (nullable CVPixelBufferRef)pixelPlayer:(nonnull PixelPlayer *)player displayBuffer:(nonnull CVPixelBufferRef)buffer {
    return [filter handleBuffer:buffer];
}

@end
